package poc.appium.serenitybdd.steps;

import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.questions.Text;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;

import java.util.List;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.hasItem;

public class AddNoteSteps {

    @Managed(driver = "Appium")
    public WebDriver mobileDriver;

    private static Question<List<String>> noteTitle() {
        return actor -> Text.of(Target.the("")
                .located(By.id("com.example.android.testing.notes:id/note_detail_title"))).viewedBy(actor).resolveAll();
    }

    private static Question<List<String>> noteDescription() {
        return actor -> Text.of(Target.the("")
                .located(By.id("com.example.android.testing.notes:id/note_detail_description"))).viewedBy(actor).resolveAll();
    }

    @Before
    public void setTheStage() {
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^that (.*) has a note list containing only the default notes$")
    public void haveTheDefaultNotes(String actor) {
        theActorCalled(actor).can(BrowseTheWeb.with(mobileDriver));
    }

    @When("^he creates a note with a picture$")
    public void createNoteWithPicture() {
        theActorInTheSpotlight().attemptsTo(
                Click.on(Target.the("to add a new note")
                        .located(By.id("com.example.android.testing.notes:id/fab_add_notes"))),
                Enter.theValue("Create a new note").into(Target.the("")
                        .located(By.id("com.example.android.testing.notes:id/add_note_title"))),
                Enter.theValue("Create a new note").into(Target.the("")
                        .located(By.id("com.example.android.testing.notes:id/add_note_description"))),
                Click.on(Target.the("")
                        .located(By.id("com.example.android.testing.notes:id/fab_add_notes")))
        );
    }

    @Then("^his note list should contains the new note$")
    public void doesTheListHasNewNote() {
        theActorInTheSpotlight().should(
                seeThat(
                        "the list of notes",
                        noteTitle(),
                        hasItem("Create a new note")
                ),
                seeThat(
                        "the detailed note",
                        noteDescription(),
                        hasItem("Create a new note")
                )
        );
    }

    @And("^the note detail should contains a picture$")
    public void doesTheNoteDetailHasThePicture() {
        throw new PendingException();
    }

}
