package poc.appium.serenitybdd.steps;

import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class SearchOnChromeSteps {

    @Managed(driver = "Appium")
    public WebDriver mobileDriver;

    @Before
    public void setTheStage() {
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^(.*) needs information about (.*)$")
    public void needToLookForInformation(String actor, String searchText) {
        theActorCalled(actor).can(BrowseTheWeb.with(mobileDriver));
        theActorInTheSpotlight().attemptsTo(Open.url("http://www.google.com"));
    }

    @When("^s?he searches more information on (.*)$")
    public void searchOnChromeForInformation(String searchText) {
        theActorInTheSpotlight().attemptsTo(
//                Enter.theValue("Serenity BDD").into(Target.the("")
//                        .located(By.id("com.android.chrome:id/search_box_text")))
//                Enter.theValue("Serenity BDD").into(Target.the("")
//                        .located(By.id("com.android.chrome:id/url_bar")))
                Enter.theValue("Serenity BDD").into(Target.the("")
                        .located(By.className("android.widget.Spinner"))),
                Click.on(Target.the("")
                        .located(By.className("android.widget.Button")))
        );
    }

    @Then("^s?he should see the result returned has information for (.*)$")
    public void hasTheSearchReturnedTheRequiredInformation(String validateSearchText) {
        throw new PendingException();
    }

}
