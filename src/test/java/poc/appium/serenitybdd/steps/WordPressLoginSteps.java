package poc.appium.serenitybdd.steps;

import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class WordPressLoginSteps {

    @Managed(driver = "Appium")
    public WebDriver mobileDriver;

    @Before
    public void setTheStage() {
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^(.*) wants to create her own site$")
    public void openTheApp(String actor) {
        theActorCalled(actor).whoCan(BrowseTheWeb.with(mobileDriver));
    }

    @When("^s?he logs into WordPress app as (.*)$")
    public void logToTheApp(String username) {
        theActorInTheSpotlight().attemptsTo(
                Click.on(Target.the("the login button")
                        .located(By.id("org.wordpress.android:id/login_button"))),
                Enter.theValue(username).into(Target.the("email address")
                        .located(By.id("org.wordpress.android:id/input"))),
                Click.on(Target.the("NEXT button")
                        .located(By.id("org.wordpress.android:id/primary_button")))
        );
    }

    @Then("^s?he should get an option to provide the password$")
    public void isTheOptionToProvidePasswordIsVisible() {
        throw new PendingException();
    }
}
